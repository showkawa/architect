package com.brian.configclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-18 20
 **/
@RestController
@RefreshScope
public class ConfigController {

    @Value("${regn:default}")
    private String  value;

    @GetMapping("/health")
    public String health(){
        return value;
    }

}
