package com.brian.apimemberserviceimpl;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2Doc
public class ApiMemberServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiMemberServiceImplApplication.class, args);
    }

}
