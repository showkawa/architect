package com.brian.apimemberserviceimpl.service.impl;

import com.brian.apimemberservice.entity.Member;
import com.brian.apimemberservice.service.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-14 18
 **/
@RestController
@Api("会员服务接口")
public class MemberServiceImpl implements MemberService {

    @GetMapping("/getMember")
    @Override
    @ApiOperation("获取会员信息")
    @ApiImplicitParam(name = "name",value = "用户名称",required = true,dataTypeClass = String.class)
    public String getMember(String name) {
        return new Member(name,18).toString();
    }


    @Override
    @GetMapping("/getString")
    @ApiOperation("测试断路器超时")
    public String getString() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "kawa";
    }
}
