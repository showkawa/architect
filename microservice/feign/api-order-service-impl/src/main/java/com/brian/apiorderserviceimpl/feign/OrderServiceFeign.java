package com.brian.apiorderserviceimpl.feign;

import com.brian.apiorderservice.service.OrderService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-14 11
 **/
@FeignClient(value = "brian-member-impl-server",fallback = OrderServiceCallBack.class)
public interface OrderServiceFeign extends OrderService {
}
