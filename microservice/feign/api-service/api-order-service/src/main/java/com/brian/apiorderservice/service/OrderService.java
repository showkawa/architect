package com.brian.apiorderservice.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-14 11
 **/
public interface OrderService {

    @GetMapping("/getMember")
    public String order2Member(@RequestParam("name") String name);

    @GetMapping("/getString")
    public String test();
}
