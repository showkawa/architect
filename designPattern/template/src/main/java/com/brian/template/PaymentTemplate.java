package com.brian.template;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;


@Slf4j
public abstract class PaymentTemplate {



    public String asyncPay(){
        //解析和验证签名
        String signature = verifySignature();
        //存日志
        saveLog(signature);
        //修改订单状态和返回支付结果
        return updateOrder();
    }

    public abstract String updateOrder();


    @Async
    public void saveLog(String signature) {
        log.info("{} 支付日志入库。。。。",signature);
    }

    public abstract String verifySignature();

    public abstract String returnSuccess();

    public abstract String returnFailed();


}
