package com.brian.template;


import org.springframework.stereotype.Component;

@Component
public class WeChatPaymentTemplate extends PaymentTemplate {
    @Override
    public String updateOrder() {
        if(true){
            return returnFailed();
        }
        return returnSuccess();
    }

    @Override
    public String verifySignature() {
      return "微信支付验证签名";
    }

    @Override
    public String returnSuccess() {
        return "微信支付成功 200";
    }

    @Override
    public String returnFailed() {
        return "微信支付失败 201";
    }
}
