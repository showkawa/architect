package com.brian.service;

import com.brian.template.PaymentTemplate;
import com.brian.utils.SpringBeanUtils;
import org.springframework.stereotype.Component;


@Component
public class TemplateFactory {

    private PaymentTemplate paymentTemplate;

    public PaymentTemplate getPaymentTemplate(String beanId){
         /*if(paymentTemplate != null){
             return paymentTemplate;
         }*/
         paymentTemplate = SpringBeanUtils.getBean(beanId, PaymentTemplate.class);
         return paymentTemplate;
   }
}
