package com.brian.responsibilitychain.handle.impl;

import com.brian.responsibilitychain.handle.GatewayHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 黑名单处理
 */
@Component
@Slf4j
public class BlacklistHandler extends GatewayHandler {
    @Override
    public void handle() {
        log.info("网关 - 黑名单处理。。。");
        nextService();
    }
}
