package com.brian.responsibilitychain.handle;

/**
 *
 */
public abstract class GatewayHandler {

    /**
     * 下一个执行的handler
     */
    private GatewayHandler nextGatewayHandler;

    public void setNextGatewayHandler(GatewayHandler nextGatewayHandler){
        this.nextGatewayHandler = nextGatewayHandler;
    }

    public abstract void handle();

    protected void nextService(){
        if(nextGatewayHandler != null){
            nextGatewayHandler.handle();
        }
    }

}
