package com.brian.responsibilitychain.factory;

import com.brian.responsibilitychain.handle.GatewayHandler;
import com.brian.responsibilitychain.handle.impl.BlacklistHandler;
import com.brian.responsibilitychain.handle.impl.ConversationHandler;
import com.brian.responsibilitychain.handle.impl.CurrentLimitHandler;

public class FactoryHandler {

    public static GatewayHandler getGatewayHandler(){
        GatewayHandler gatewayHandler1 = new CurrentLimitHandler();
        GatewayHandler gatewayHandler2 = new BlacklistHandler();
        gatewayHandler1.setNextGatewayHandler(gatewayHandler2);
        GatewayHandler gatewayHandler3 = new ConversationHandler();
        gatewayHandler2.setNextGatewayHandler(gatewayHandler3);
        return gatewayHandler1;
    }
}
