package com.brian.responsibilitychain.controller;


import com.brian.responsibilitychain.factory.FactoryHandler;
import com.brian.responsibilitychain.handle.GatewayHandler;
import com.brian.responsibilitychain.service.GatewayHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HandlerController {

    @Autowired
    private GatewayHandlerService gatewayHandlerService;

    @GetMapping("/handle")
    public ResponseEntity<String> handle(){
       // GatewayHandler handler = FactoryHandler.getGatewayHandler();
        GatewayHandler handler = gatewayHandlerService.getFirstGatewayHandler();
        handler.handle();
        return new ResponseEntity<String>("ok",HttpStatus.OK);
    }


    @GetMapping("/")
    public ResponseEntity<String> test(){
        return new ResponseEntity<String>("8082",HttpStatus.OK);
    }
}
