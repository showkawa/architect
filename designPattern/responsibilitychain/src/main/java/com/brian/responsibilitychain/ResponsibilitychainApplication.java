package com.brian.responsibilitychain;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.brian.responsibilitychain.mapper")
public class ResponsibilitychainApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResponsibilitychainApplication.class, args);
    }

}
