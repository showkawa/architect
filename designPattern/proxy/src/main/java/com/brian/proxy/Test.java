package com.brian.proxy;

import com.brian.proxy.proxy.MyProxy;
import com.brian.proxy.service.OrderService;
import com.brian.proxy.service.impl.BrianJdkInvocationhandler;
import com.brian.proxy.service.impl.JavaClassLoader;
import com.brian.proxy.service.impl.OrderServiceImpl;

import java.lang.reflect.InvocationTargetException;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-20 21
 **/
public class Test {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
       // OrderService proxy = new $Proxy2(new BrianJdkInvocationhandler(new OrderServiceImpl()));
        //proxy.order();

        OrderService o =
                (OrderService) MyProxy.newProxyInstance(new JavaClassLoader(), OrderService.class, new BrianJdkInvocationhandler(new OrderServiceImpl()));
        o.order();
    }
}
