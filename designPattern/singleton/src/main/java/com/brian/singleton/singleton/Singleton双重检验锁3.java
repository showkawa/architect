package com.brian.singleton.singleton;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-30 21
 *
 * 双重检验锁相比懒汉式，在实例化类的时候才加锁在读的时候不加锁,相比懒汉式减少了线程在读时候等待的问题
 * 但是在实例化的过程加了锁还是存在线程等待的问题
 *
 **/
public class Singleton双重检验锁3 {
    private volatile static Singleton双重检验锁3 singleton双重检验锁3;


    //构造函数私有化
    private Singleton双重检验锁3() {
    }


    public  static Singleton双重检验锁3 getSingleton双重检验锁3() {

        if(singleton双重检验锁3 == null){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (Singleton双重检验锁3.class){
                if(singleton双重检验锁3 == null){
                    singleton双重检验锁3 = new Singleton双重检验锁3();
                }
            }
        }
        return singleton双重检验锁3;
    }

    public static void main(String[] args) {

        for (int i = 0; i < 20; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Singleton双重检验锁3 双重检验锁 = Singleton双重检验锁3.getSingleton双重检验锁3();

                    System.out.println(Thread.currentThread().getName() + 双重检验锁);
                }
            }).start();

        }
    }

}
