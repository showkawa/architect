package com.brian.singleton.singleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-30 22
 **/
public enum Singleton7 {
    SINGLETON;

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        System.out.println("----getName----: " +name);
        return name;
    }

    public static void main(String[] args) throws Exception {
        Singleton7 枚举单例1 = Singleton7.SINGLETON;
        Singleton7 枚举单例2 = Singleton7.SINGLETON;
        枚举单例1.setName("kawa");
        枚举单例2.getName();
        System.out.println(枚举单例1 == 枚举单例2);


        //反射机制破解 会直接抛出异常 NoSuchMethodException
        Constructor<Singleton7> constructor = Singleton7.class.getDeclaredConstructor(String.class,int.class);
        constructor.setAccessible(true);
        Singleton7 枚举单例3 = constructor.newInstance("SINGLETON",1);
        System.out.println(枚举单例3 == 枚举单例2);


    }
}
