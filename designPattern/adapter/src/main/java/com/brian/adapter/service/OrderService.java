package com.brian.adapter.service;

import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-28 20
 **/
@Service
public class OrderService {

    /**
     * V1.1 版本入参是Map,新版本V1.2需要支持List
     */
    public void order4Map(Map<String, Object> map){
        for (int i = 0; i < map.size(); i++) {
            System.out.println("-----> " + map.get(i));
        }
    }
}
