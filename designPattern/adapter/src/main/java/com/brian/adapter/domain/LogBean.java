package com.brian.adapter.domain;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-02 11
 **/
public class LogBean {
    /**
     * 日志ID
     */
    private String logId;
    /**
     * 日志内容
     */
    private String logText;

    public String getLogId() {
        return logId;
    }

    public String getLogText() {
        return logText;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"logId\":\"")
                .append(logId).append('\"');
        sb.append(",\"logText\":\"")
                .append(logText).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
