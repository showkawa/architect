package com.brian.adapter;

import com.brian.adapter.adapter.ListAdapter;
import com.brian.adapter.domain.LogBean;
import com.brian.adapter.service.LogWriteFileService;
import com.brian.adapter.service.OrderService;
import com.brian.adapter.service.impl.LogAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-28 21
 **/
public class TestAdapter {

    public static void main(String[] args) {

        List list = new ArrayList<>();
        list.add("k1");
        list.add("k2");
        list.add("k3");
        ListAdapter listAdapter = new ListAdapter(list);
        OrderService orderService =  new OrderService();
        orderService.order4Map(listAdapter);

        LogAdapter adapter = new LogAdapter(new LogWriteFileService());
        LogBean logBean = new LogBean();
        logBean.setLogId("10088");
        logBean.setLogText("日志信息。。。");
        adapter.writeDbFile(logBean);


    }
}
