package com.brian.decorative.factory;

import com.brian.decorative.service.GatewayComponent;
import com.brian.decorative.service.impl.BasicComponentGateway;
import com.brian.decorative.service.impl.LimitDecorative;
import com.brian.decorative.service.impl.LogDecorative;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:38
 **/
public class FactoryGateway {

    public static GatewayComponent getGatewayComponent() {
        return new LimitDecorative(new LogDecorative(new BasicComponentGateway()));
    }


    public static void main(String[] args) {
        FactoryGateway.getGatewayComponent().service();
    }
}
