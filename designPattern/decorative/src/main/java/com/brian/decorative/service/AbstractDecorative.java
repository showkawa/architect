package com.brian.decorative.service;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:13
 **/
public abstract class AbstractDecorative extends GatewayComponent {

    private GatewayComponent gatewayComponent;

    public AbstractDecorative(GatewayComponent gatewayComponent) {
        this.gatewayComponent = gatewayComponent;
    }

    @Override
    public void service() {
        if(gatewayComponent !=null){
            gatewayComponent.service();
        }
    }
}
