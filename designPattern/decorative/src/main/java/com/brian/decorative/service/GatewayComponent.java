package com.brian.decorative.service;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:08
 **/
public abstract class GatewayComponent {

    public abstract void service();
}
