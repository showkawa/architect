package com.brian.decorative.service.impl;

import com.brian.decorative.service.AbstractDecorative;
import com.brian.decorative.service.GatewayComponent;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:16
 **/
@Slf4j
public class LogDecorative extends AbstractDecorative {


    public LogDecorative(GatewayComponent gatewayComponent) {
        super(gatewayComponent);
    }

    @Override
    public void service() {
        super.service();
        log.info("2-->>> 网关中新增日志收集..");
    }
}
