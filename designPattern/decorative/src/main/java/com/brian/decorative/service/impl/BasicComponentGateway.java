package com.brian.decorative.service.impl;

import com.brian.decorative.service.GatewayComponent;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:15
 **/
@Slf4j
public class BasicComponentGateway  extends GatewayComponent {

    @Override
    public void service() {
        log.info("1-->>> 网关中获取基本的操作...");
    }
}
