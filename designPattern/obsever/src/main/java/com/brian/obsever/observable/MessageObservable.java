package com.brian.obsever.observable;

import com.fasterxml.jackson.databind.util.JSONPObject;

import java.util.Observable;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-23 22
 **/
public class MessageObservable extends Observable {

    private JSONPObject jsonpObject;



    @Override
    public void notifyObservers(Object arg) {
        //1.修改状态开启群发
        setChanged();
        //2.调用父类的notifyObservers群发消息
        super.notifyObservers(arg);
    }
}
