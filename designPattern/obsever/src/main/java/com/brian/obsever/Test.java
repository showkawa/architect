package com.brian.obsever;

import com.brian.obsever.observable.MessageObservable;
import com.brian.obsever.observable.observer.EmailObserver;
import com.brian.obsever.observable.observer.SmsObserver;
import com.brian.obsever.observable.observer.WechatObserver;
import com.brian.obsever.observer.impl.UserObserver;
import com.brian.obsever.subject.AbstractSubject;
import com.brian.obsever.subject.impl.WechatSubject;

import java.util.Observable;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-22 22
 **/
public class Test {

    public static void main(String[] args) {
        //1.创建具体的主题
       // AbstractSubject subject = new WechatSubject();
        //2.添加观察者
      //  subject.addObserver(new UserObserver("用户A"));
      //  subject.addObserver(new UserObserver("用户B"));
        //3.发布消息
       // subject.notifyObseever("收到公司内部晋升邮件");

        /*基于JDK自带的Observer*/
        Observable messageObservable = new MessageObservable();

        messageObservable.addObserver(new SmsObserver());
        messageObservable.addObserver(new EmailObserver());
        messageObservable.addObserver(new WechatObserver());
        messageObservable.notifyObservers("许多云下单成功！！");

    }
}
