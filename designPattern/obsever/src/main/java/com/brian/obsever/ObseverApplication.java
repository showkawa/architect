package com.brian.obsever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ObseverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObseverApplication.class, args);
    }

}
