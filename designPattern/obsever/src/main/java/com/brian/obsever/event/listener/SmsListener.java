package com.brian.obsever.event.listener;

import com.brian.obsever.event.OrderMessageEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-24 09
 **/
@Component
public class SmsListener implements ApplicationListener<OrderMessageEvent> {


    @Override
    @Async
    public void onApplicationEvent(OrderMessageEvent orderMessageEvent) {
        System.out.println(Thread.currentThread().getName() + " 发送短信消息：" + orderMessageEvent.getMap().toString());
    }
}
