package com.brian.obsever.observer.impl;

import com.brian.obsever.observer.Observer;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect 具体的订阅
 * @author: Brian Huang
 * @create: 2019-05-22 22
 **/
@Slf4j
public class UserObserver implements Observer {

    private String name;

    public UserObserver(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {
        log.info(name + " <> " + message);
    }
}
