package com.brian.obsever.controller;

import com.brian.obsever.event.OrderMessageEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-24 09
 **/
@RestController
public class OrderController {

    @Autowired
    private ApplicationContext applicationContext;


    @GetMapping("/order")
    public ResponseEntity<?> order(){
        Map<String, String> map = new HashMap<>();
        map.put("orderId","QW12345676545");
        map.put("content","Redis实战");
        map.put("price","$43");
        OrderMessageEvent orderMessageEvent = new OrderMessageEvent(this,map);

        applicationContext.publishEvent(orderMessageEvent);

        return new ResponseEntity<>(map, HttpStatus.OK);

    }
}
