package com.brian.obsever.observable.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-23 22
 **/
public class SmsObserver implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("用户下单成功，发送短息内容：" + arg);
    }
}
