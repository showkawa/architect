package com.brian.obsever.subject.impl;

import com.brian.obsever.observer.Observer;
import com.brian.obsever.subject.AbstractSubject;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang  具体的主题
 * @create: 2019-05-22 22
 **/
public class WechatSubject extends AbstractSubject {

    private List<Observer> observerList = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObseever(String message) {
        observerList.forEach(observer -> {
            observer.update(message);
        });
    }
}
