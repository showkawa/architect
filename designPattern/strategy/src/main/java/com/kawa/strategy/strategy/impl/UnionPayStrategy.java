package com.kawa.strategy.strategy.impl;

import com.kawa.strategy.strategy.PayStrategy;
import org.springframework.stereotype.Component;

/**
 * 银联支付
 */
@Component
public class UnionPayStrategy implements PayStrategy {
    @Override
    public String toPay() {
        return "调用银联支付。。。";
    }
}
