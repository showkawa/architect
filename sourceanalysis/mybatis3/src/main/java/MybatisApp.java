import mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-04 22
 **/
public class MybatisApp {

    public static void main(String[] args) {
        try {
            //1.定义配置文件
            String resource = "mybatis.xml";
            //2.获取InputStreamReader IO流
            Reader reader = Resources.getResourceAsReader(resource);
            //3.获取SqlSessionFactory
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            //4.获取Session
            SqlSession session = sqlSessionFactory.openSession();
            //5.执行mapper接口方法
            UserMapper mapper = session.getMapper(UserMapper.class);
            System.out.println("--result--: " + mapper.getUser(1).toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
