package com.brian.spring5.aop.interceptor.impl;

import com.brian.spring5.aop.interceptor.MethodInterceptor;
import com.brian.spring5.aop.invocation.MethodInvocation;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/
public class AfterMethodInterceptor implements MethodInterceptor {
    public void invoke(MethodInvocation methodInvocation) {
        methodInvocation.process();//目标方法
        System.out.println("后置通知。。。。");

    }
}
