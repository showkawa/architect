package com.brian.spring5.v2.config;

import com.brian.spring5.v1.entity.UserEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Service;


@Configuration
@ComponentScan(value = "com.brian.spring5.v2", excludeFilters =
        {@ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Service.class)}, useDefaultFilters = true)
public class MySpringConfig {
    // includeFilters 包含的扫包范围 必须有这些 排除这些没有
    //@Configuration 等于 spring xml
    // <bean class="UserEntity" id="方法名称" >
    //  将该包下 的 只要有类上加上@service 注解的 注入到spring容器
    @Bean
    public UserEntity userEntity() {
        return new UserEntity("showkawa", 10010);
    }
    /**
     *  如果需要将外部的jar包注入spring容器中 @Bean
     */
}
