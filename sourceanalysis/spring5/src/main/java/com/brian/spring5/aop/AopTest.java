package com.brian.spring5.aop;


import com.brian.spring5.aop.interceptor.MethodInterceptor;
import com.brian.spring5.aop.interceptor.impl.AfterMethodInterceptor;
import com.brian.spring5.aop.interceptor.impl.BeforeMethodInterceptor;
import com.brian.spring5.aop.invocation.DefaultMethodInvocation;
import com.brian.spring5.aop.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/
public class AopTest {

    public static void main(String[] args) {
       List<MethodInterceptor> methodInterceptorList = new ArrayList<MethodInterceptor>();
       methodInterceptorList.add(new BeforeMethodInterceptor());
       methodInterceptorList.add(new AfterMethodInterceptor());
        UserService userService = new UserService();

        DefaultMethodInvocation defaultMethodInvocation = new DefaultMethodInvocation(methodInterceptorList, userService);

        defaultMethodInvocation.process();
    }
}
