package com.brian.spring5.aop.interceptor.impl;

import com.brian.spring5.aop.interceptor.MethodInterceptor;
import com.brian.spring5.aop.invocation.MethodInvocation;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/
public class BeforeMethodInterceptor implements MethodInterceptor {
    public void invoke(MethodInvocation methodInvocation) {

        System.out.println("前置通知。。。。");
        methodInvocation.process();

    }
}
