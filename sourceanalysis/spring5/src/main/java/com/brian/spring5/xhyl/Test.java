package com.brian.spring5.xhyl;

import com.brian.spring5.xhyl.config.MainConfig;
import com.brian.spring5.xhyl.service.AaaService;
import com.brian.spring5.xhyl.service.BbbService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-13 20
 **/
public class Test {

    public static void main(String[] args) {

        ApplicationContext context =
                new AnnotationConfigApplicationContext(MainConfig.class);
        AaaService aaaService = context.getBean(AaaService.class);
        BbbService bbbService = context.getBean(BbbService.class);

        aaaService.setBbbService(bbbService);
        bbbService.setAaaService(aaaService);


    }
    /*
    * 1.bean初始化的单例的时候，不会出现循环依赖，底层是Spring三级缓存保证不会循环依赖
    * 2.多例情况下，会出现循环依赖的问题，因为多例的情况下，创建多个对象，依赖的时候不能确认依赖哪个对象
    *
    * */

    /*
    * 多例的请情况下，可以通过set方法设置具体要依赖的bean,从而可以解决循环依赖的问题
    * */
}
