### 天道酬勤,一步一个坑

#### 1.设计模式 designPatternt
##### 1.1 策略模式  strategy
##### 1.2 责任链模式  responsibilitychain
##### 1.3 模板方法模式  template
##### 1.4 装饰器模式 decorative
##### 1.5 代理模式 proxy
##### 1.6 观察者模式 observer
##### 1.7 单例模式  singleton
##### 1.8 适配器模式 adapter

#### 2.微服务SpringCloud2.X microservice
##### 2.1 服务治理 eureka
##### 2.2 DiscoveryClient实现负载均衡，轮询模式
##### 2.3 feign客户端调用，给人感觉就像调用本地服务一样
##### 2.4 config分布式配置中心
##### 2.5 apollo分布式配置中心
##### 2.6 zuul动态网关和统一API管理
##### 2.7 config分布式配置中心 + bus消息总线实现全局微服务的通知
##### 2.8 zipkin链路追踪 主要是微服务复杂调用的时候方便第一时间快速定位问题

#### 3.互联网高并发场景 concurrent
##### 3.1 服务保护断路器 hystrix
##### 3.2 服务限流 ratelimit
##### 3.3 rabbitMQ 消息幂等性设计和分布式事务处理  rabbitmq
##### 3.4 lcn 分布式事务
##### 3.5 sharding-jdbc 分库分表

#### 4.互联网安全架构 security
##### 4.1 预防xss跨站脚本攻击 xss
##### 4.2 防盗链技术 doorchain
##### 4.3 接口幂等性设计 idempotent
##### 4.3 springboot security细粒度基于页面的权限控制

#### 5.源码分析
##### 5.1 mybatis 3.x源码分析
###### 5.1.1 SqlSessionFactory源码分析
###### 5.1.2 Mapper与接口绑定源码分析

##### 5.2 Spring5源码分析
###### 5.2.1 Spring5源码深度解析(一)之理解Configuration注解   https://www.cnblogs.com/hlkawa/p/11085020.html
###### 5.2.2 Spring5源码深度分析(二)之理解@Conditional,@Import注解   https://www.cnblogs.com/hlkawa/p/11088129.html
###### 5.2.3 Spring5深度源码分析(三)之AnnotationConfigApplicationContext启动原理分析  https://www.cnblogs.com/hlkawa/p/11100604.html

#### 6 mongo-demo 
##### 6.1 手动封装BrianCache注解，实现 ehcache + redis的一二级缓存
##### 6.2 redis实现分布式锁 setnx
##### 6.3 redis生成全局Id  incr固定加1 incrby固定步长 天生具有原子性
##### 6.4 雪花算法 生成全局ID
