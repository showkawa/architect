package com.brian.idempotent.util;

import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-28 15
 **/
public class TokenUtils {

   private static Map<String,Object> tokens =  new ConcurrentHashMap<>();

    public static String getToken(){
        String token = "kawa+" + System.currentTimeMillis();
        tokens.put(token,token);
        return token;
    }

    public static boolean findToken(String token){
        if(!tokens.containsKey(token)){
            return false;
        }
        Object to= tokens.get(token);
        if(StringUtils.isEmpty(to)){
            return false;
        }
        tokens.remove(token);
        return true;
    }
}
