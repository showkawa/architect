package com.brian.springsecurity.mapper;

import com.brian.springsecurity.entity.Permission;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface PermissionMapper {

	@Select(" select * from sys_permission ")
	List<Permission> findAllPermission();

}
