package com.brian.doorchain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class DoorchainApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoorchainApplication.class, args);
    }

}
