package com.brian.mongo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-17 16:23
 **/
@Configuration
@EnableSwagger2
public class Swagger2Config {

        //启动项目swagger访问地址;http://localhost:8088/swagger-ui.html#/

        //swagger2的配置文件，这里可以配置swagger2的一些基本的内容，比如扫描的包等等
        @Bean
        public Docket createRestApi() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    //为当前包路径
                    .apis(RequestHandlerSelectors.basePackage("com.brian.mongo.controller"))
                    .paths(PathSelectors.any())
                    .build();
        }
        //构建 api文档的详细信息函数,注意这里的注解引用的是哪个
        private ApiInfo apiInfo() {
            return new ApiInfoBuilder()
                    //页面标题
                    .title("Spring Boot 测试使用 Swagger2 构建RESTful API")
                    //创建人
                    .contact(new Contact("cookie", "http://www.baidu.com", "1092249319@qq.com"))
                    //版本号
                    .version("0.0.1")
                    //描述
                    .description("用户管理")
                    .build();
        }

    }

