package com.brian.mongo;

import com.brian.mongo.config.JwtFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.servlet.Filter;

@SpringBootApplication
@EnableCaching
public class MongoApplication {


    @Bean
    public FilterRegistrationBean jwtFilter(){

        FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new JwtFilter());
        filterRegistrationBean.addUrlPatterns("/aop/v1/*");
        return filterRegistrationBean;
    }


    @Bean
    public StringRedisTemplate customStringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        StringRedisTemplate template = new StringRedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        //开启redis事务
        template.setEnableTransactionSupport(true);
        return template;
    }

    public static void main(String[] args) {
        SpringApplication.run(MongoApplication.class, args);
    }

}
