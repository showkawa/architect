package com.brian.mongo.repository;

import com.brian.mongo.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User ,String> {

    @Query("{'username':{$in:[?0]}, 'password':{$in:[?1]}}")
    public User validateUser(String username, String password);

    @Query("{'username':{$in:[?0]}}")
    public List<User> findUserByUsername(String username);


}
