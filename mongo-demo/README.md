### MongoDB + JWT / Redis

#### 1.mongodb + jwt  实现登陆验证
#### 2.使用docker容器打包发布项目

#### 3.redis
##### 3.1 redis分库，默认是16个库，一般会根据不用的项目，连接不同的库
##### 3.2 手动封装BrianCache注解，实现 ehcache + redis的一二级缓存
##### 3.3 redis实现分布式锁 setnx
##### 3.4 redis生成全局Id  incr固定加1 incrby固定步长 天生具有原子性
##### 3.5 雪花算法 生成全局ID

##### springboot2-redis锁的源码地址： https://github.com/spring-projects/spring-integration/tree/master/spring-integration-redis/src/main/java/org/springframework/integration/redis
##### 目标源码分析 核心setnx 获取锁前的超时设置 获取锁后的超时设置
