package com.brian.mqproducer.controller;

import com.brian.mqproducer.base.BaseApiService;
import com.brian.mqproducer.base.ResponseBase;
import com.brian.mqproducer.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-16 22
 **/
@RestController
public class OrderController extends BaseApiService {
    @Autowired
    private OrderService orderService;

    @RequestMapping("/addOrder")
    public ResponseBase addOrder() {
        return orderService.addOrderAndDispatch();
    }

}
