package com.brian.mqconsumer.mapper;

import com.brian.mqconsumer.entity.DispatchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-17 20
 **/
@Repository
public interface DispatchMapper  extends JpaRepository<DispatchEntity,Long> {
}
