package com.brian.ratelimit.annotation;

import java.lang.annotation.*;

/**
 * @program: architect 自定义服务限流注解框架 底层是基于 AOP + RateLimiter
 * @author: Brian Huang
 * @create: 2019-05-25 22
 **/

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BrianLimit {

    /**
     * 以每秒固定速度向令牌桶中添加令牌
     * @return
     */
    double permitsPerSecond() default 1.0;

    /**
     * 在规定时间内没有获取到令牌，走服务降级程序
     * @return
     */
    long timeout() default 500l;


}
