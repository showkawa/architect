package com.kawa.shardingjdbc.config;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.table.SingleKeyTableShardingAlgorithm;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

//分表策略
// 地区亚洲  AS 落到 t_order_18
// 地区北美  NA 落到 t_order_35
// 地区欧洲  EU 落到 t_order_45
// 地区澳洲  OS 落到 t_order_60
// 地区非洲  UR 落到 t_order_80
public class TableShardingAlgorithm implements SingleKeyTableShardingAlgorithm<String> {

	private static Map<String, String> map =  new HashMap<>();

	static {
		map.put("AS","AS");
		map.put("NA","NA");
		map.put("EU","EU");
		map.put("OS","OS");
		map.put("UR","UR");
	}

	// sql 中关键字 匹配符为 =的时候，表的路由函数
	public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<String> shardingValue) {

		for (String tableName : availableTargetNames) {
			//分表策略
			// 地区亚洲  AS 落到 t_order_AS
			// 地区北美  NA 落到 t_order_NA
			// 地区欧洲  EU 落到 t_order_EU
			// 地区澳洲  OS 落到 t_order_OS
			// 地区非洲  UR 落到 t_order_UR

			if (tableName.endsWith(shardingValue.getValue())) {
				return tableName;
			}
		}

		throw new IllegalArgumentException();
	}

	@Override
	public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<String> shardingValue) {

		Collection<String> result = new LinkedHashSet<>(availableTargetNames.size());
		for (String tableName : availableTargetNames) {
			//分表策略
			// 地区亚洲  AS 落到 t_order_AS
			// 地区北美  NA 落到 t_order_NA
			// 地区欧洲  EU 落到 t_order_EU
			// 地区澳洲  OS 落到 t_order_OS
			// 地区非洲  UR 落到 t_order_UR

			if (tableName.endsWith(shardingValue.getValue())) {
				result.add(tableName);
			}
		}
		return result;
	}

	@Override
	public Collection<String> doBetweenSharding(Collection<String> availableTargetNames,
			ShardingValue<String> shardingValue) {

		Collection<String> result = new LinkedHashSet<>(availableTargetNames.size());
		for (String tableName : availableTargetNames) {
			//分表策略
			// 地区亚洲  AS 落到 t_order_AS
			// 地区北美  NA 落到 t_order_NA
			// 地区欧洲  EU 落到 t_order_EU
			// 地区澳洲  OS 落到 t_order_OS
			// 地区非洲  UR 落到 t_order_UR

			if (tableName.endsWith(shardingValue.getValue())) {
				result.add(tableName);
			}
		}
		return result;
	}

}
