package com.brian.rabbitmq.domain;

import lombok.Data;

import java.util.Date;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-11 09
 **/
@Data
public class MessageEntity {

    private long id = System.currentTimeMillis();
    private String title;
    private String content;
    private Date createDate = new Date();
    private String createBy = "Brian";

    public MessageEntity() {
    }

    public MessageEntity(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"id\":")
                .append(id);
        sb.append(",\"title\":\"")
                .append(title).append('\"');
        sb.append(",\"content\":\"")
                .append(content).append('\"');
        sb.append(",\"createDate\":\"")
                .append(createDate).append('\"');
        sb.append(",\"createBy\":\"")
                .append(createBy).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
