package com.brian.rabbitmq.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-15 14
 **/
@Service
public class MessageIdService {


    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    public String putId(){
        String id =  "KAWA-" + System.currentTimeMillis();
        redisTemplate.opsForValue().set(id,id);
        return id;
    }

    public String getId(String id){
        String messageId = redisTemplate.opsForValue().get(id);
        return messageId;
    }

    public String deleteId(String id){
        redisTemplate.delete(id);
        return id;
    }
}
