package com.brian.rabbitmq.controller;

import com.brian.rabbitmq.service.FanoutProducer;
import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-11 09
 **/
@Controller
public class FanoutController {

    @Autowired
    private FanoutProducer fanoutProducer;

    @GetMapping("/sendMessage")
    public ResponseEntity<?> sendMessage(@RequestParam String queueName){
        fanoutProducer.sendMessage(queueName);

        return new ResponseEntity<>("成功投递消息到: " + queueName, HttpStatus.OK);
    }
}
