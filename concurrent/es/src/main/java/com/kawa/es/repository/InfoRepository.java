package com.kawa.es.repository;

import com.kawa.es.domain.InfoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-08-12 22
 **/
@Repository
public interface InfoRepository extends CrudRepository<InfoEntity, String> {
}
