package com.brian.lcn4xstockserver.service;

import com.brian.lcn4xstockserver.base.ResponseBase;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-22 21
 **/
public interface StockService {

    // 根据商品id 减库存数量
    @RequestMapping("/inventoryReduction")
    public ResponseBase inventoryReduction(@RequestParam("commodityId") Long commodityId);
}
